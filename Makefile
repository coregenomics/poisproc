.PHONY : summary
summary : ../../bin/stansummary output.csv
	$^

../../bin/stansummary :
	$(MAKE) -C ../.. bin/stansummary

output.csv : poisproc poisproc.R
	./$< sample data file=$<.R

%.R : %-data-generator.R
	Rscript $<

% : %.stan
	$(MAKE) -C ../.. examples/$@/$@
