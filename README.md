# CmdStan Poisson process example code

Untar the [CmdStan release](https://github.com/stan-dev/cmdstan/releases) and
in the `examples/` directory clone this repository.  Then inside the cloned
directory, run `make` to build and run.
