## Save data to be input to Stan model.
set.seed(123)
lambda <- rnorm(1, mean = 5, sd = 0.5)
lambda
N <- 10000
counts <- rpois(N, lambda = lambda)
summary(counts)

## Export data for CmdStan.
rstan::stan_rdump(list("N", "counts"), file = "poisproc.R")
