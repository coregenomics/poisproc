data {
  int<lower = 1> N;
  int<lower = 0> counts[N];
}
parameters {
  real<lower = 0> lambda;
}
model {
  /* Assuming a normal intensity distribtion applies to microarray data, not
     next-generation sequencing; better would be a negative binomial, maybe but
     would need to an appropriate empirical prior. */
  target += normal_lpdf(lambda | 5, 0.5);
  target += poisson_lpmf(counts | lambda);
}
